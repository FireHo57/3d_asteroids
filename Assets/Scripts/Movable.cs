﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is a component that moves things that own it
public class Movable : MonoBehaviour
{
    public struct Acceleration
    {
        //value in m/s^2
        public Vector3 value;
        // time to accelerate for
        public float time;
    }

    public void Awake()
    {
        m_accelerationProfile = new List<Acceleration>();
    }

    private Vector3 m_currentVelocity; 
    private List<Acceleration> m_accelerationProfile;

    public void AddAcceleration(List<Acceleration> acceleration)
    {
        m_accelerationProfile.AddRange(acceleration);
    }

    /*Methods that called by RunForFrames*/
    private void Move(float timeSinceLastFrame)
    {
        transform.position += m_currentVelocity * timeSinceLastFrame;
    }

    //adds some acceleration to the current velocity
    private void IncrementAcceleration(float timeSinceLastFrame)
    {
        if( m_accelerationProfile.Count > 0 )
        {
            Acceleration current = m_accelerationProfile[0];
            m_currentVelocity += current.value * timeSinceLastFrame;
            current.time -= timeSinceLastFrame;
            if(current.time < 0 )
            {
                m_accelerationProfile.RemoveAt(0);
            }
        }
    }

    public void RunForFrames(float timeSinceLastFrame)
    {
        Move(timeSinceLastFrame);
        IncrementAcceleration(timeSinceLastFrame);
    }
}
