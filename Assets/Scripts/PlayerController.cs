﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    Vector3 m_currentSpeed;

    [SerializeField]
    float m_acceleration;

    [SerializeField]
    Vector3 m_rotateSpeed;

    [SerializeField]
    [HideInInspector]
    Forecaster m_foreCaster;
    [SerializeField]
    [HideInInspector]
    OrderManager m_Manager;

    [SerializeField]
    int m_Granularity;

    Rigidbody m_rigid;

    private Movable m_moveable;

    public void Awake()
    {
        m_moveable = gameObject.AddComponent<Movable>();
        m_Manager = GetComponentInChildren<OrderManager>();
        m_foreCaster = GetComponentInChildren<Forecaster>();
        m_rigid = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }  

    // Update is called once per frame
    void Update()
    {
    }

    public void RunForTime(float timeSinceLastFrame)
    {
        m_moveable.RunForFrames(timeSinceLastFrame);
    }

    public void AddRotate()
    {
        Debug.Log("Adding Rotate");
        m_Manager.m_orderList.Add(new OrderManager.Order(Rotate, 1.0f, "Rotate"));
        List<Forecaster.PositionAndRotation> positions = m_foreCaster.m_forecastPositions;
        Vector3 startPosition = transform.position;
        Vector3 startSpeed = m_currentSpeed;
        Quaternion startRotation = transform.rotation;
        if ( positions.Count > 0 )
        {
            Forecaster.PositionAndRotation last = positions[positions.Count-1];
            startPosition = last.m_position;
            startRotation = last.m_rotation;
            startSpeed = last.m_velocity;
        }
        positions.Add(new Forecaster.PositionAndRotation(startPosition, startRotation, startSpeed));
        Vector3 startEuler = startRotation.eulerAngles;
        startEuler += m_rotateSpeed*1.0f;
        Quaternion endRot = Quaternion.Euler(startEuler);
        Vector3 endPosition = startPosition + startSpeed;
        positions.Add(new Forecaster.PositionAndRotation(endPosition, endRot, startSpeed));
        m_foreCaster.AddForecast(positions);
    }

    public void AddAcceleration(int accelerationValue , int accelerationTime)
    { 
        m_Manager.m_orderList.Add(new OrderManager.Order((float timeSinceLastFrame)=>
            {
                Accelerate(accelerationValue/accelerationTime, timeSinceLastFrame);
            },
        accelerationTime, "Accelerate"));
        List<Forecaster.PositionAndRotation> positions = m_foreCaster.m_forecastPositions;
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.localRotation;
        Vector3 startSpeed = m_currentSpeed;
        if (positions.Count > 0)
        {
            Forecaster.PositionAndRotation latest = positions[positions.Count - 1];
            startPosition = latest.m_position;
            startRotation = latest.m_rotation;
            startSpeed = latest.m_velocity;
        }

        positions.Add(new Forecaster.PositionAndRotation(startPosition, startRotation, startSpeed));
        // as acceleration causes non linear pathing do it by rough increments
        Vector3 acceleratedSpeed = startSpeed;
        Vector3 acceleratedPosition = startPosition;
        float incrementAccel = accelerationValue / m_Granularity;
        float stepIncrement = (float)accelerationTime / (float)m_Granularity;
        for (int i = 0; i < m_Granularity; ++i)
        {
            acceleratedSpeed += stepIncrement * accelerationValue * transform.forward;
            acceleratedPosition += acceleratedSpeed*stepIncrement;
            positions.Add(new Forecaster.PositionAndRotation(acceleratedPosition, startRotation, acceleratedSpeed));
        }

        m_foreCaster.AddForecast(positions);
        Debug.Log("Transform forward: " + transform.forward);
    }

    public void AddDeceleration()
    {
        Debug.Log("Adding deceleration");
        m_Manager.m_orderList.Add(new OrderManager.Order(Decelerate, 1.0f, "Accelerate"));
    }

    void Rotate(float timeSinceLastFrame )
    {
        transform.Rotate(m_rotateSpeed*timeSinceLastFrame);
        Move(timeSinceLastFrame);
    }

    void Accelerate(int accelerationValue, float timeSinceLastFrame)
    {
        m_currentSpeed += timeSinceLastFrame * accelerationValue * transform.forward;
        Move(timeSinceLastFrame);
    }

    void Accelerate(float timeSinceLastFrame)
    {
        m_currentSpeed += timeSinceLastFrame * m_acceleration * transform.forward;
        Move(timeSinceLastFrame);
    }

    void Decelerate(float timeSinceLastFrame)
    {
        m_currentSpeed -= timeSinceLastFrame * m_acceleration*transform.forward;
        Move(timeSinceLastFrame);
    }

    void Nothing(float timeSinceLastFrame)
    {
        //do nothing
        Move(timeSinceLastFrame);
    }



    
}
