﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AccelerationDialogManager : MonoBehaviour
{
    [SerializeField]
    GameObject UiElement;

    GameObject ui;

    struct AccelerationProfile
    {
        public int AccelerationValue;
        public int AccelerationTime;
    }

    private AccelerationProfile m_AccelerationProfile;
    private GameObject player;

    public void Awake()
    {
        //create acceleration profile
        m_AccelerationProfile = new AccelerationProfile();

        //grab stuff
        player = GameObject.Find("Player");

        //create the dialog
        ui = Instantiate(UiElement);
        //parent it to the canvas so that it can b seen
        ui.transform.parent = GameObject.Find("Canvas").transform;
        //put it in the middle of the screen
        ui.transform.position = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);

        //Set up the input callbacks
        TMP_InputField[] inputs = ui.GetComponentsInChildren<TMP_InputField>();
        foreach (TMP_InputField uiElement in inputs)
        {
            if (uiElement.name == "AccelInput")
            {
                TMP_InputField input = uiElement.GetComponent<TMP_InputField>();
                input.onValueChanged.AddListener(HandleAccelerationChanged);
            }
            else if (uiElement.name == "TimeInput")
            {
                TMP_InputField input = uiElement.GetComponent<TMP_InputField>();
                input.onValueChanged.AddListener(HandleAccelerationTimeChanged);
            }
        }

        // set up the button callbacks
        Button[] buttons = ui.GetComponentsInChildren<Button>();
        buttons[0].onClick.AddListener(HandleAccelerationOkClicked);
        buttons[1].onClick.AddListener(HandleAccelerationCancelClicked);
    }

    public void HandleAccelerationChanged(string newAcceleration)
    {
        int accel_int = System.Int16.Parse(newAcceleration);
        m_AccelerationProfile.AccelerationValue = accel_int;
    }

    public void HandleAccelerationTimeChanged(string newTime)
    {
        int time_int = System.Int16.Parse(newTime);
        m_AccelerationProfile.AccelerationTime = time_int;
    }

    public void HandleAccelerationCancelClicked()
    {
        Destroy(ui);
    }

    public void HandleAccelerationOkClicked()
    {
        Debug.Log("Handling Acceleration clicked");
        PlayerController pc = player.GetComponent<PlayerController>();
        pc.AddAcceleration(m_AccelerationProfile.AccelerationValue, m_AccelerationProfile.AccelerationTime);
        Destroy(ui);
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
