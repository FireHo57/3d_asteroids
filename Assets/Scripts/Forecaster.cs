﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Forecaster : MonoBehaviour
{
    //steps to take for each move
    [SerializeField]
    int m_granularity;

    public struct PositionAndRotation
    {
        public PositionAndRotation(Vector3 position, Quaternion rotation, Vector3 velocity)
        {
            m_position = position;
            m_rotation = rotation;
            m_velocity = velocity;
        }

        public Vector3 m_position;
        public Vector3 m_velocity;
        public Quaternion m_rotation;
    }

    public List<PositionAndRotation> m_forecastPositions;

    LineRenderer m_lineRenderer;

    public void Awake()
    {
        m_lineRenderer = GetComponent<LineRenderer>();
        m_forecastPositions = new List<PositionAndRotation>();
    }

    public void AddForecast(List<PositionAndRotation> positionList)
    {
        m_lineRenderer.positionCount = positionList.Count;
        List<Vector3> lineList = new List<Vector3>();
        foreach (PositionAndRotation posAndRot in positionList)
        {
            lineList.Add(posAndRot.m_position);
        }

        m_lineRenderer.SetPositions(lineList.ToArray());
    }

    public void ClearForecast()
    {
        m_lineRenderer.positionCount = 0;
        m_lineRenderer.SetPositions(new Vector3[0]);
        m_forecastPositions = new List<PositionAndRotation>();
    }
    /*
    public void UpdateForecast()
    {
        //clone parent object
        GameObject tempParent = Instantiate(transform.parent.gameObject);
        List<OrderManager.Order> orderList = tempParent.GetComponentInChildren<OrderManager>().m_orderList;
        List<Vector3> positionList = new List<Vector3>();
        positionList.Add(tempParent.transform.position);

        foreach( OrderManager.Order order in orderList )
        {
            float stepTime = order.m_time / m_granularity;
            float elapsedTime = 0.0f;
            while ( elapsedTime < order.m_time)
            {
                order.m_thingToRun(stepTime);
                positionList.Add(tempParent.transform.position);
            }
        }

        m_lineRenderer.positionCount = positionList.Count;
        m_lineRenderer.SetPositions(positionList.ToArray());
        Destroy(tempParent);
    }*/

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
