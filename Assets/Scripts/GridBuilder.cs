﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBuilder : MonoBehaviour
{
    [SerializeField]
    Vector3Int m_gridSize;

    [SerializeField]
    Vector3 m_cubeSize;

    [SerializeField]
    Vector3 m_startPosition;

    [SerializeField]
    GameObject CubePrefab;

    List<List<List<GameObject>>> m_gridPositions;

    GameObject GridHolder;
    // Start is called before the first frame update
    void Start()
    {
        GridHolder = new GameObject();

        m_gridPositions = new List<List<List<GameObject>>>();
        float gridSizeActualX = m_gridSize.x*m_cubeSize.x;
        float gridSizeActualY = m_gridSize.y*m_cubeSize.y;
        float gridSizeActualZ = m_gridSize.z*m_cubeSize.z;

        for(int i = 0; i < m_gridSize.x; i++)
        {
            List<List<GameObject>> yList = new List<List<GameObject>>();
            for(int j = 0; j < m_gridSize.y; j++)
            {
                List<GameObject> zList = new List<GameObject>();
                for (int k = 0; k < m_gridSize.z; k++)
                {
                    float xPos = m_startPosition.x+i*m_cubeSize.x;
                    float yPos = m_startPosition.y+j*m_cubeSize.y;
                    float zPos = m_startPosition.z+k*m_cubeSize.z;

                    zList.Add(null);
                }
                yList.Add(zList);
            }
            m_gridPositions.Add(yList);
        }

        Debug.Log(m_gridPositions);
    }

    public GameObject GetCubeAt(Vector3 worldPosition)
    {
        Vector3 relPosition = worldPosition - m_startPosition;
        int divX = (int)Mathf.Floor(relPosition.x/m_cubeSize.x);
        int divY = (int)Mathf.Floor(relPosition.y/m_cubeSize.y);
        int divZ = (int)Mathf.Floor(relPosition.z/m_cubeSize.z);

        GameObject cube = m_gridPositions[divX][divY][divZ];
        if( cube == null )
        {
            m_gridPositions[divX][divY][divZ] = GameObject.Instantiate(CubePrefab, 
                                                                       new Vector3(divX,divY,divZ),
                                                                       Quaternion.identity,
                                                                       GridHolder.transform);
        }

        return cube;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
