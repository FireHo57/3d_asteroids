﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class OrderManager : MonoBehaviour
{
    public void Awake()
    {
        m_orderList = new List<Order>();
    }
    /*
     * Defines a thing to be run for a time
     */
    public struct Order
    {
        public float m_time;
        public Runner.ThingToRun m_thingToRun;
        public string m_name;
        public bool m_isMove;

        public Order(Runner.ThingToRun ttr, float time, string name, bool isMove = false)
        {
            m_thingToRun = ttr;
            m_time = time;
            m_name = name;
            m_isMove = isMove;
        }
    }

    public List<Order> m_orderList;

    public void RunOrders(float timeSinceLastFrame)
    {
        if (m_orderList.Count > 0)
        {
            //take the next thing off the list and do it
            Order task = m_orderList[0];

            task.m_thingToRun(timeSinceLastFrame);
            task.m_time -= timeSinceLastFrame;
            Debug.Log("Ran " + task.m_name + " for " + timeSinceLastFrame + " there is " + task.m_time + " time left");
            if (task.m_time < 0)
            {
                m_orderList.RemoveAt(0);
            }
            else
            {
                m_orderList[0] = task;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
