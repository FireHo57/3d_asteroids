﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    GameObject Player;

    [SerializeField]
    GameObject Runner;


    // Start is called before the first frame update
    void Start()
    {
        Runner runner = Instantiate(Runner).GetComponent<Runner>();
        runner.Init();
        PlayerController player = GameObject.Find("Player").GetComponent<PlayerController>();
        runner.AddThingToRun(player.GetComponentInChildren<OrderManager>().RunOrders);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
