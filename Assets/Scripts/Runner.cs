﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Runner : MonoBehaviour
{
    [SerializeField]
    float TimeToRun;

    public delegate void ThingToRun(float timeSinceLastFrame);

    private List<ThingToRun> m_listOfThingsToRun;

    private enum State
    {
        RUNNING = 0,
        PLANNING = 1
    }

    private State m_currentState;
    private float m_timeLeftToRun;

    public void AddThingToRun(ThingToRun newThingToRun)
    {
        m_listOfThingsToRun.Add(newThingToRun);
    }

    public void Init()
    {
        m_currentState = State.PLANNING;
        m_listOfThingsToRun = new List<ThingToRun>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (m_currentState == State.RUNNING)
        {
            m_timeLeftToRun -= Time.deltaTime;
            foreach(ThingToRun t in m_listOfThingsToRun)
            {
                t(Time.deltaTime);
            }
            if( m_timeLeftToRun < 0 )
            {
                m_currentState = State.PLANNING;
                GameObject.Find("Forecaster").GetComponent<Forecaster>().ClearForecast();
            }
        }
        else
        {
            if( Input.GetKeyDown(KeyCode.Space) )
            {
                m_currentState = State.RUNNING;
                m_timeLeftToRun = TimeToRun;
            }
        }
    }
}
